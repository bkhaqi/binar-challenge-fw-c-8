const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

//Express Basic Auth
const basicAuth = require('express-basic-auth')
let adminAuth = basicAuth({
  users : {'admin': 'admin'},
  challenge: true
});

//Swagger UI
const swaggerUI = require("swagger-ui-express");
const swaggerJSON = require("./docs/swagger/swagger.json");
app.use("/api-docs",adminAuth, swaggerUI.serve, swaggerUI.setup(swaggerJSON));
//-------------------------------------------------------------------

// accept request in form or JSON
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const db = require("./app/models");
db.client.sync();

require("./app/routes/player.routes")(app);

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
