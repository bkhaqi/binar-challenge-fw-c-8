import React from 'react'

function Result ({ResultText, username, email, exp, lvl}) {
    return (
        <>
            <div className="card text-bg-secondary mb-3" style={{maxWidth: "18rem"}}>
                <div className="card-header">{ResultText}</div>
                <div className="card-body">
                    <p className="card-text">Username : {username}</p>
                    <p className="card-text">Email : {email}</p>
                    <p className="card-text">Exp : {exp}</p>
                    <p className="card-text">Level : {lvl}</p>
                </div>
            </div>
        </>
    )
}

export default Result