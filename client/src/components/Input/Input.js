import React from 'react'

function Input ({type, placeholder, value, handlePlayerChange}) {
    return (
        <>
            <input
            style={{ maxWidth: "400px", margin: 'auto' }}
            className='form-control my-4'
            autoComplete='nope'
            type={type}
            placeholder={placeholder}
            value={value}
            onChange={handlePlayerChange}
            />
        </>
    );
}

export default Input