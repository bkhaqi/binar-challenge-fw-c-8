import React from 'react'

function Button ({className, handleOnClickButton, someText}) {
    return (
        <button 
                className={className}
                onClick={handleOnClickButton}
                type='submit'
                
        >{someText}</button> 
    );
}

export default Button