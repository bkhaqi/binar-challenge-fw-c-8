import React ,{  useState } from "react";
import Input from "./components/Input/Input";
import Button from "./components/Input/Button";
import Result from "./components/Result/Result";

function App() {

  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [exp, setExp] = useState(0);
  const [lvl, setLvl] = useState(0);

  const[buttonState, setButtonState] = useState('');

  let [show, setShow] = useState('');

  //form input text change
  function handleUsernameChange(e){
    e.preventDefault()
    setUsername(e.target.value)
    setButtonState('')
  }

  function handleEmailChange(e){
    e.preventDefault()
    setEmail(e.target.value);
    setButtonState('');
  }

  function handleExpChange(e){
    e.preventDefault()
    setExp(e.target.value)
    setButtonState('');
  }

  function handleLvlChange(e) {
    e.preventDefault()
    setLvl(e.target.value)
    setButtonState('');
  }

  //submit form 
  function handlePlayerSubmit (e) {
    e.preventDefault()
    setShow('1')
    console.log (show)

    
  }

  //submit button
  function handleCreateButton(e)  {
    e.preventDefault()
    setShow('')
    setButtonState('Player Created')
  }

  function handleEditButton(e) {
    e.preventDefault()
    setShow('')
    setButtonState('Player Edit')
  }

  function handleSearchButton(e) {
    e.preventDefault()
    setShow('')
    setButtonState('Result Player')
  }

  // if-else to control show
  const conditions = [buttonState !== '', username !== '', email !== '', exp !== '', lvl !== '']
  const allConditionsMet = conditions.every(condition => condition)
  let newShow;
  if (allConditionsMet){
    newShow = <Result ResultText={buttonState}
                      username={username}
                      email={email}
                      exp={exp}
                      lvl={lvl}
              />
  } else {
    if (buttonState ){
      newShow = <>Invalid Player Data</>
    } else {
      newShow = <></>
    }
    
    
  }

  //Clear Player Data
  function handleClearButton (e){
    e.preventDefault()
    setButtonState('')
    setShow('')
    setUsername('')
    setEmail('')
    setExp('')
    setLvl('')
  }


  return (
  <div className="container my-5">
    <h3 className="my-3">Binar Challenge C8</h3>
    <div className="row">
      <div className="col-6 text-center">
          <form 
                style={{width: '450px'}}
                className="form-control bg-secondary bg-gradient"
                onSubmit={handlePlayerSubmit}
                
          >
            <Input 
                  type='text'
                  placeholder='Username'
                  value={username}
                  handlePlayerChange={handleUsernameChange}
            />
            <Input 
                  type='email'
                  placeholder='Email'
                  value={email}
                  handlePlayerChange={handleEmailChange}
            />
            <Input 
                  type='number'
                  placeholder='exp'
                  value={exp}
                  handlePlayerChange={handleExpChange}
            />
            <Input 
                  type='number'
                  placeholder='lvl'
                  value={lvl}
                  handlePlayerChange={handleLvlChange}
            />
            <Button className='btn btn-primary mx-2 my-4'
                    handleOnClickButton={handleCreateButton}
                    someText='➕'
            />
            <Button className='btn btn-dark mx-2 my-4'
                    handleOnClickButton={handleSearchButton}
                    someText='🔍'
            />
            <Button className='btn btn-warning mx-2 my-4'
                    handleOnClickButton={handleEditButton}
                    someText='📝'
            />
            <Button className='btn btn-danger mx-2 my-4'
                    handleOnClickButton={handleClearButton}
                    someText='Clear'
            />
          </form>
        </div>
        <div className="col-6">
          {newShow}
        </div>
      </div>
  </div>
  )
}

export default App;
